# Custom Bike Computer

So. Apps like Komoot and Strava are nice, but they only show aggregated data.
I'd like to have some more details and more functionality.

## Cadence Sensor with ESP-12F

Actual used code is in `esp-code/cadence-display-interrupts/*.ino`

Inside `esp-code/cadence-with-display/` is code from first learning how to control sensor and display. It failed in real world usage due to timing issues (as expected).

### Sensors

joy-it.net/sensorkit

- ky-021 mini magnet reed modul

### Display

i2c oled 128x64

- <https://www.instructables.com/id/Monochrome-096-i2c-OLED-display-with-arduino-SSD13/>
- Arduino IDE: Library Manager > SD1306 for ESP8266 or ESP32
- fonts: <http://oleddisplay.squix.ch/#/home>
- API DisplayControl <https://github.com/ThingPulse/esp8266-oled-ssd1306#api>
- API OLEDDisplayUI <https://github.com/ThingPulse/esp8266-oled-ssd1306#ui-library-oleddisplayui>

#### Connections

| nodeMCUv1.0 pin | device            |
|-----------------|-------------------|
| 3.3v            | ky-021 VCC        |
| GND             | ky-021 - (GND)    |
| D1              | ky-021 S (signal) |
|-----------------|-------------------|
| 3.3v            | SD1306 VCC        |
| GND             | SD1306 GND        |
| D3              | SD1306 SDA        |
| D5              | SD1306 SCK        |
|-----------------|-------------------|

### Software

- Arduino IDE
  - <https://github.com/arduino/Arduino/blob/master/build/shared/manpage.adoc>
  - *TODO* maybe move to VSCodium + <https://github.com/arduino/arduino-cli>
- 3rd Party Board Support Libraries
  - <https://github.com/arduino/Arduino/wiki/Unofficial-list-of-3rd-party-boards-support-urls>
  - community libraries: <https://arduino.esp8266.com/stable/package_esp8266com_index.json>
- <https://www.instructables.com/id/ESP-12F-ESP8266-Module-Minimal-Breadboard-for-Flas/>
- ESP 8266 Reference: <http://arduino.esp8266.com/Arduino/versions/2.0.0/doc/reference.html>

### ESP 8266 NodeMCU v1 Pins

<https://techtutorialsx.com/2017/04/02/esp8266-nodemcu-pin-mappings/>

    D0 = GPIO16;
    D1 = GPIO5;
    D2 = GPIO4;
    D3 = GPIO0;
    D4 = GPIO2;
    D5 = GPIO14;
    D6 = GPIO12;
    D7 = GPIO13;
    D8 = GPIO15;
    D9 = GPIO3;
    D10 = GPIO1;
    LED_BUILTIN = GPIO16 (auxiliary constant for the board LED, not a board pin);
