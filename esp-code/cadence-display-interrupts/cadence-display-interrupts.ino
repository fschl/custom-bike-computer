/*
  ESP8266 cadence display by fschl
  Blink the blue LED on the ESP-01 module
  This example code is in the public domain
*/
// https://circuits4you.com/2019/01/09/esp32-oled-library-display-example/
// #include "display-ssd1306.ino"
#include "OLEDDisplayUi.h"
#include "SSD1306Wire.h"

// Initialize the OLED display using Wire library
#define HALLPIN D1
#define SCK D5 
#define SDA D3 
SSD1306Wire  display(0x3c, SDA, SCK);
#define COUNT_MEASUREMENTS 120
#define COUNT_MAGNETS 1
#define DEBUG_DISPLAY 1

// values to display
float currentCadence = 0.0;
float avg10sec = 0.0;
float maxCadence = 0.0;
float totalCyclingDuration= 0.0;

// sharedState
int firstPass = 0;
int lastPass = 0;
int numPasses = 0;
int passTimes[COUNT_MEASUREMENTS] = {};

// UI components for Display
void msOverlay() {
  display.setTextAlignment(TEXT_ALIGN_RIGHT);
  display.drawString(128, 0, String(millis()/100));
}

void drawFrameCadenceUI() {
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_16);
  display.drawString(0, 2, "curr:" + String(currentCadence));

  display.setFont(ArialMT_Plain_10);
  display.drawString(0, 22 , "10s avg:" + String(avg10sec));
  display.drawString(0, 34 , "max:" + String(maxCadence));
  display.drawString(0, 48, "Tour: " + String(totalCyclingDuration) + "s");
  display.setTextAlignment(TEXT_ALIGN_RIGHT);
  display.drawString(127, 22, "start@" + String(firstPass) + "s");
  display.drawString(127, 34, "last: " + String(lastPass));
  display.drawString(127, 48 , "cnt:" + String(numPasses));
}

void setup() {
  Serial.begin(115200);
  Serial.println();
  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);

  attachInterrupt(HALLPIN, registerSensorPass, FALLING);
}

ICACHE_RAM_ATTR void registerSensorPass() {
  Serial.println("sensor falling...");
  numPasses++;
  if (firstPass == 0) {
    firstPass = millis() / 1000;
    lastPass = millis();
  }
  shiftData();
}

void loop() {
  if (passTimes[0] > 0) {
    calculateCadence();
  }
  display.clear();
  msOverlay();
  drawFrameCadenceUI();
  display.display();
  // Serial.println(String(digitalRead(HALLPIN)));
  delay(1000);
}
