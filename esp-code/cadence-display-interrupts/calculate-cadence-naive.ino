void calculateCadence() {
  int now = millis();

  currentCadence = (60 * 1000.0 / passTimes[0]) / COUNT_MAGNETS;
  Serial.println(String(passTimes[0]) + " => " + String(currentCadence));
  if (maxCadence < currentCadence) {
    maxCadence = currentCadence;
  }

  int numRot10s = 0;
  int totalMovingAverageTime = 0;
  for (int i = 0; i < COUNT_MEASUREMENTS && totalMovingAverageTime < 10000; i++) {
    numRot10s++;
    totalMovingAverageTime += passTimes[i];
  }

  avg10sec = (numRot10s / totalMovingAverageTime) * 6000.0;
  totalCyclingDuration = (1.0*(now - firstPass)) / 1000.0;
}

ICACHE_RAM_ATTR void shiftData() {
  int currentTime = millis();
  int timeDiff = currentTime - lastPass;
  lastPass = currentTime;
  if (timeDiff > 300) {
    for (int i = COUNT_MEASUREMENTS - 1; i > 0; i--) {
      passTimes[i] = passTimes[i - 1];
    }
    passTimes[0] = timeDiff;
  }
}