# Transport over WIFI+MQTT

## Resources

- <https://github.com/knolleary/pubsubclient>
  - <https://pubsubclient.knolleary.net/api.html> 
- for mqtt client on esp <https://electrosome.com/esp8266-mqtt-client-arduino-iot/>
- <https://girishjoshi.io/post/golang-paho-mqtt/>

## prepare DevEnv on Laptop

`docker run -it -p 1883:1883 -p 9001:9001 -v mosquitto.conf:/mosquitto/config/mosquitto.conf eclipse-mosquitto`


## Improvement Ideas

Maybe try protobuf later. It's said to be more efficient <https://medium.com/grpc/efficient-iot-with-the-esp8266-protocol-buffers-grafana-go-and-kubernetes-a2ae214dbd29> 
