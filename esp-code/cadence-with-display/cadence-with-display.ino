/*
  ESP8266 cadence display by fschl
  Blink the blue LED on the ESP-01 module
  This example code is in the public domain
*/
// https://circuits4you.com/2019/01/09/esp32-oled-library-display-example/
// #include "display-ssd1306.ino"
#include "OLEDDisplayUi.h"
#include "SSD1306Wire.h"

// Initialize the OLED display using Wire library
#define HALLPIN D1
#define SCK D5 
#define SDA D3 
SSD1306Wire  display(0x3c, SDA, SCK);
OLEDDisplayUi ui( &display );

void setup() {
  Serial.begin(115200);
  Serial.println();

  setupDisplay();
}

void loop() {
  int remainingTimeBudget = ui.update();

  if (remainingTimeBudget > 0) {
    // You can do some work here don't do stuff if you are below your
    // time budget.
    readSensor();
    shiftData();

    delay(remainingTimeBudget);
  } else {
    Serial.println("no time for redraw");
  }
}
