// HALL sensor signal

// prepare calculation of cadence
#define COUNT_MEASUREMENTS 120
#define TARGET_FPS 20
#define COUNT_MAGNETS 1
#define DEBUG_DISPLAY 1

float values[COUNT_MEASUREMENTS] = {};
int times[COUNT_MEASUREMENTS] = {};

int numPasses() {
  int numRotations = 0;
  for (int i = 0; i < COUNT_MEASUREMENTS; i++) {
    if (values[i] > 500) {
      numRotations++;
    }
  }
  return numRotations;
}

float calculateCadence() {
  float cadence = 0.0f;
  int millisOfCalc = millis() - times[0];
  int numRotations = numPasses() / COUNT_MAGNETS;
  if (numRotations > 0) {
    // if 6000ms duration, we hade x rotations / 6sek
    // avg 10*x rorations / 10sek
    cadence = (float)(10000 * numRotations ) / (1.0 * millisOfCalc);
  }
  return cadence;
}

float readSensor() {
    // sensor shows 1023 if magnet absent, 0 if magnet present
    return 1050 - analogRead(HALLPIN);
}

void shiftData() {
    int currentTime = millis();
    for (int i = 0; i < COUNT_MEASUREMENTS - 1; i++) {
      values[i] = values[i + 1];
      times[i] = times[i + 1];
    }
    values[COUNT_MEASUREMENTS - 1] = readSensor();
    times[COUNT_MEASUREMENTS - 1] = currentTime;
}