
// https://circuits4you.com/2019/01/09/esp32-oled-library-display-example/

// UI components for Display
void msOverlay(OLEDDisplay *display, OLEDDisplayUiState* state) {
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  // display->setFont(ArialMT_Plain_10);
  display->drawString(128, 0, String(millis()/100));
}

void drawBars(OLEDDisplay *display) {

  int bar_size;
  int index = 0;
  for (int i = 0; i < 60; i++) {
    index = COUNT_MEASUREMENTS - i;
    if (index > 0) {
      if (values[index] < 500) {
        bar_size = 15;
      } else {
        bar_size = 3;
      }
      display->drawVerticalLine(i * 2, 48, bar_size);
    }
  }
}

void drawFrameCadenceUI(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_16);
  String cadenceString = "Cad:";
  cadenceString += String(calculateCadence());

  display->drawString(0 + x, 2 + y, cadenceString);

  // The coordinates define the left starting point of the text
  display->setFont(ArialMT_Plain_10);
  int now = millis();
  int first_diff = now - times[0];
  display->drawString(0 + x, 32 + y, String(first_diff));
  display->drawString(100 + x, 32 + y, String(numPasses()));

    drawBars(display);
}

FrameCallback frames[] = { drawFrameCadenceUI };
int frameCount = 1;

// Overlays are statically drawn on top of a frame eg. a clock
OverlayCallback overlays[] = { msOverlay };
int overlaysCount = 1;

void setupDisplay() {
  Serial.println("setting up display...");

  ui.setTargetFPS(TARGET_FPS);
  ui.disableAllIndicators();
  ui.disableAutoTransition();

  // Add frames and Overlays
  ui.setFrames(frames, frameCount);
  ui.setOverlays(overlays, overlaysCount);
  ui.init();

  Serial.println("..done");
}